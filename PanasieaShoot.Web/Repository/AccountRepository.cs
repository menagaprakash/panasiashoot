﻿
using PanasieaShoot.Web.Data;
using PanasieaShoot.Web.Map;
using PanasieaShoot.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
namespace PanasieaShoot.Web.Repository
{
    public class AccountRepository
    {
        DatabaseDataContext dbcontext = new DatabaseDataContext();
        public AccountModel Login(string userName, string password)
        {
           var status = Membership.ValidateUser(userName, password);           
          
            AccountModel model = null;
            if (status)
            {
                var membershipUser = Membership.GetUser(userName);
                if (membershipUser != null)
                {
                    var account = dbcontext.Accounts.FirstOrDefault(x => x.UserId == (int)membershipUser.ProviderUserKey);
                    if (account != null)
                        model = AccountMap.ToModel(account);
                }
            }
            return model;

        }

        public bool IsUserExist(string userName)
        {
            return dbcontext.Accounts.Any(x => x.EmailId == userName);          
        }

        public string Register(AccountModel model)
        {
            if (!IsUserExist(model.EmailId))
            {
                MembershipCreateStatus status;
                var user = Membership.CreateUser(model.EmailId, model.Password, model.EmailId, null, null, true, out status);

                if (status != MembershipCreateStatus.Success)
                {
                    return "Could not create user.";
                }
               
                model.UserId = (int)user.ProviderUserKey;

                try
                {
                    dbcontext.Accounts.InsertOnSubmit(AccountMap.ToEntity(model));

                    dbcontext.SubmitChanges();

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
              
            }

            return string.Empty;
        }

        public AccountModel GetAccount(string userName)
        {
            var membershipUser = Membership.GetUser(userName);
            if (membershipUser != null)
            {
                var account = dbcontext.Accounts.FirstOrDefault(x => x.UserId == (int)membershipUser.ProviderUserKey);
                if (account != null)
                   return AccountMap.ToModel(account);
            }
            return null;
        }



    }
}
