﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasieaShoot.Web.Models
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Name is required")]
        [DisplayName("Name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Email is required")]
         [DisplayName("Email")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email Format is wrong")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
         [DisplayName("Phone number")]
        public string Phone { get; set; }

         [DisplayName("Company Name")]
        public string Company { get; set; }

         [DisplayName("Website")]
        public string Website { get; set; }

         [DisplayName("Question/Comments")]
        public string Message { get; set; }
    }
}
