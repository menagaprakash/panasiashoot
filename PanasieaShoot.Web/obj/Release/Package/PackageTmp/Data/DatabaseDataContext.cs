﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PanasieaShoot.Web.Data
{
    public partial class DatabaseDataContext
    {
        public DatabaseDataContext()
            : base(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString(), mappingSource)
        {
            OnCreated();
        }
    }
}