﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasieaShoot.Web.Models
{
    public class UserAccountModel
    {
        [Required(ErrorMessage = "User name Required:")]
        [DisplayName("User Name:")]
        [StringLength(30, ErrorMessage = "Less than 30 characters")]
        public string Username { get; set; }


        [Required(ErrorMessage = "Password Required:")]
        [DataType(DataType.Password)]
        [DisplayName("Password:")]
        [StringLength(30, ErrorMessage = "Less than 30 characters")]
        public string Password { get; set; }


        [System.ComponentModel.DataAnnotations.Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
    }

    public class AccountModel
    {
        [Required(ErrorMessage = "FirstName Required:")]
        [DisplayName("First Name:")]
        [RegularExpression(@"^[a-zA-Z'.\s]{1,40}$", ErrorMessage = "Special Characters not allowed")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName Required:")]
        [RegularExpression(@"^[a-zA-Z'.\s]{1,40}$", ErrorMessage = "Special Charactersnot allowed")]
        [DisplayName("Last Name:")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "EmailId Required:")]
        [DisplayName("Email Id:")]
        //[RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email Format is wrong")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string EmailId { get; set; }


       
        //[DisplayName("Confirm Email Id:")]
        //[RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email Format is wrong")]
        //[StringLength(100, ErrorMessage = "Less than 100 characters")]
        //[Compare("EmailId", ErrorMessage = "Confirm email not matched.")]
        public string ConfirmEmailId { get; set; }

        [Required(ErrorMessage = "Password Required:")]
        [DataType(DataType.Password)]
        [DisplayName("Password:")]
        [StringLength(30, ErrorMessage = "Less than 30 characters")]
        public string Password { get; set; }

        
        //[DataType(DataType.Password)]
        //[Compare("Password", ErrorMessage = "Confirm not matched.")]
        //[Display(Name = "Confirm password:")]
        //[StringLength(30, ErrorMessage = "Less than 30 characters")]
        public string ConfirmPassword { get; set; }

        //[Required(ErrorMessage = "Street Address Required")]
        [DisplayName("Street Address1:")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string StreetAddress1 { get; set; }

        [DisplayName("Street Address2:")]
        [StringLength(100, ErrorMessage = "Less than 100 characters")]
        public string StreetAddress2 { get; set; }

        //[Required(ErrorMessage = "City Required")]
        [DisplayName("City:")]
        [RegularExpression(@"^[a-zA-Z'.\s]{1,40}$", ErrorMessage = "Special Characters not allowed")]
        [StringLength(50, ErrorMessage = "Less than 50 characters")]
        public string City { get; set; }

        //[Required(ErrorMessage = "State Required")]
        [DisplayName("State:")]
        [RegularExpression(@"^[a-zA-Z'.\s]{1,40}$", ErrorMessage = "Special Characters not allowed")]
        [StringLength(50, ErrorMessage = "Less than 50 characters")]
        public string State { get; set; }

        //[Required(ErrorMessage = "ZipCode Required")]
        [DisplayName("Zip Code:")]
        [StringLength(20, ErrorMessage = "Less than 20 characters")]
        public string ZipCode { get; set; }

        //[DisplayName("Nick Name:")]
        public string NickName { get; set; }
        [DisplayName("Phone Number:")]
        public string PhoneNumber { get; set; }

        public int UserId { get; set; }

         [DisplayName("Country:")]
        public string Country { get; set; }

    }
}
