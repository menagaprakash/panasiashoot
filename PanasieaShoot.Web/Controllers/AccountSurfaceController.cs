﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanasieaShoot.Web.Models;
using System.Web.Security;
using PanasieaShoot.Web.Repository;
namespace PanasieaShoot.Web.Controller
{
    public class AccountController : Umbraco.Web.Mvc.SurfaceController
    {
        AccountRepository repository = new AccountRepository();


        [HttpGet]
        public ActionResult MemberLogout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            Response.Redirect("/customer-login");
            return RedirectToRoute("home");   
        }

     
        [HttpPost]
        [ActionName("MemberLogin")]
        public ActionResult MemberLoginPost(UserAccountModel model)
        {
            if (ModelState.IsValid)
            {
                var account=repository.Login(model.Username, model.Password);
                if (account!=null)
                {
                    FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);
                    Response.Redirect("/Myaccount");
                   
                }
                else
                {
                    TempData["Status"] = "Invalid username or password";
                    return RedirectToCurrentUmbracoPage();
                }
            }

            TempData["Status"] = "Invalid request";
            return RedirectToCurrentUmbracoPage();
        }

        [HttpPost]
        [ActionName("Register")]
        public ActionResult Register(AccountModel model)
        {
            if (ModelState.IsValid)
            {
                var message = repository.Register(model);
                if (string.IsNullOrEmpty(message))
                {
                    FormsAuthentication.SetAuthCookie(model.EmailId,false);
                    Response.Redirect("/Myaccount");
                }
                else
                    TempData["Message"] = message;
            }
            return RedirectToCurrentUmbracoPage();
        }

        public PartialViewResult MyAccount()
        {
            var account =new AccountModel();
            if (User.Identity.IsAuthenticated)
                account = repository.GetAccount(User.Identity.Name);
            else
                Response.Redirect("/customer-login");
            return PartialView("Account", account);
        }
    }
}
