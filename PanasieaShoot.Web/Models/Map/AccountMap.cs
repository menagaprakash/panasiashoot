﻿using PanasieaShoot.Web.Data;
using PanasieaShoot.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasieaShoot.Web.Map
{
    public static class AccountMap
    {
        public static AccountModel ToModel(Account account)
        {
            if (account == null)
                return null;
            AccountModel model = new AccountModel();
            model.FirstName = account.FirstName;
            model.LastName = account.LastName;
            model.NickName = account.NickName;
            model.EmailId = account.EmailId;
            model.PhoneNumber = account.PhoneNumber;
            model.StreetAddress1 = account.StreetAddress1;
            model.StreetAddress2 = account.StreetAddress2;
            model.City = account.City;
            model.State = account.State;
            model.Country = account.Country;
            model.ZipCode = account.ZipCode;
            model.UserId = account.UserId;
            return model;
        }

        public static Account ToEntity(AccountModel model)
        {
            if (model == null)
                return null;
            Account entity = new Account();
            entity.FirstName = model.FirstName;
            entity.LastName = model.LastName;
            entity.NickName = model.NickName;
            entity.EmailId = model.EmailId;
            entity.PhoneNumber = model.PhoneNumber;
            entity.StreetAddress1 = model.StreetAddress1;
            entity.StreetAddress2 = model.StreetAddress2;
            entity.City = model.City;
            entity.State = model.State;
            entity.Country = model.Country;
            entity.ZipCode = model.ZipCode;
            entity.UserId = model.UserId;
            return entity;
        }
    }
}
