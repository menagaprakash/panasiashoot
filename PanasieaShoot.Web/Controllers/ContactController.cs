﻿using PanasieaShoot.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;

namespace PanasieaShoot.Web.Controllers
{
    public class ContactController : Umbraco.Web.Mvc.SurfaceController
    {



        [HttpPost]
        public ActionResult ContactForm(ContactModel model)
        {


            if (ModelState.IsValid)
            {
                var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());

                var sendEmailsFrom = currentNode.GetProperty("EmailFrom").HasValue ? currentNode.GetProperty("EmailFrom").Value.ToString() : "test@test.com";
                var sendEmailsTo = currentNode.GetProperty("EmailTo").HasValue ? currentNode.GetProperty("EmailTo").Value.ToString() : "test@test.com";

                var body = String.Format("From: {0}, Email: {1}, Phone: {2}, Company: {3}, WebSite: {4} and Message: {5}", model.FullName, model.Email, model.Phone, model.Company, model.Website, model.Message);
                var subject = "Contact Page - Message Sent";


                try
                {
                    umbraco.library.SendMail(sendEmailsFrom, sendEmailsTo, subject, body, true);

                    TempData["InfoMessage"] = "Your message has been successfully sent and we will be in touch soon...";

                    // Clear all the form fields
                    ModelState.Clear();

                    //redirect to current page to clear the form
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = ex.Message + ex.StackTrace;
                }
            }

            return CurrentUmbracoPage();

        }

    }
}